﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Twilio;
using Twilio.Types;

namespace Diferenciais
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void BtnEnviar_Click(object sender, EventArgs e)
        {
            string telPara = txtPara.Text;
            string mensagem = txtMensagem.Text;

            //this.EnviarSMS(telPara, mensagem);
            this.EnviarWhatsapp(telPara, mensagem);
        }



        private void EnviarSMS(string telPara, string mensagem)
        {
            telPara = telPara.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "");

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            TwilioClient.Init("AC581e34c07ccfe2db21df73cc40970963", "e51e0d1a4ce6b72e731ed572b3523d92");

            var message = Twilio.Rest.Api.V2010.Account.MessageResource.Create(
                new PhoneNumber(telPara),
                from: new PhoneNumber("+15753030595"),
                body: mensagem
            );

        }


        private void EnviarWhatsapp(string telPara, string mensagem)
        {
            telPara = telPara.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "");

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            TwilioClient.Init("AC581e34c07ccfe2db21df73cc40970963", "e51e0d1a4ce6b72e731ed572b3523d92");

            var message = Twilio.Rest.Api.V2010.Account.MessageResource.Create(
                new PhoneNumber("whatsapp:" + telPara),
                from: "whatsapp:+14155238886",
                body: mensagem
            );

        }


    }
}
