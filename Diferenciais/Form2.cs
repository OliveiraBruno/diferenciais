﻿using IBM.Cloud.SDK.Core.Authentication.Iam;
using IBM.Watson.SpeechToText.v1;
using IBM.Watson.TextToSpeech.v1;
using Stannieman.AudioPlayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace Diferenciais
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }


        private void BtnOuvir_Click(object sender, EventArgs e)
        {
            string texto = txtFrase.Text;

            this.FalarTextoDigitado(texto);
        }


        private async void FalarTextoDigitado(string texto)
        {
            IamAuthenticator authenticator = new IamAuthenticator(
                apikey: "BO3oD40LmLUAG1yFlLm4JiNXO3E8qVoT5grUWZkDlAWi");

            TextToSpeechService service = new TextToSpeechService(authenticator);
            service.SetServiceUrl("https://stream.watsonplatform.net/text-to-speech/api");

            var result = service.Synthesize(
                text: texto,
                accept: "audio/wav",
                voice: "pt-BR_IsabelaVoice"
                );

            using (FileStream fs = File.Create("nsf_texto_voz.wav"))
            {
                result.Result.WriteTo(fs);
                fs.Close();
                result.Result.Close();
            }

            AudioPlayer player = new AudioPlayer();
            await player.SetFileAsync("nsf_texto_voz.wav", "nsf_texto_voz.wav");
            await player.PlayAsync();
        }






        private void BtnGravar_Click(object sender, EventArgs e)
        {
            mciSendString("open new Type waveaudio alias meuaudio", null, 0, IntPtr.Zero);
            mciSendString("record meuaudio", null, 0, IntPtr.Zero);

        }

        private void BtnParar_Click(object sender, EventArgs e)
        {
            mciSendString("save meuaudio nsf_voz_texto.wav", null, 0, IntPtr.Zero);
            mciSendString("close meuaudio", null, 0, IntPtr.Zero);


            IamAuthenticator authenticator = new IamAuthenticator(
                apikey: "h5d_M9jzlOU9UjzNR7Lk4Iemg12tpdq05b2Ju98hafmG");

            SpeechToTextService service = new SpeechToTextService(authenticator);
            service.SetServiceUrl("https://stream.watsonplatform.net/speech-to-text/api");

            
            var result = service.Recognize(
                audio: File.ReadAllBytes("nsf_voz_texto.wav"),
                contentType: "audio/wav",
                model: "pt-BR_NarrowbandModel"
                );

            string textoIbm = result.Result.Results.First().Alternatives.First().Transcript;

            lblTexto.Text = textoIbm;
        }





        [System.Runtime.InteropServices.DllImport("winmm.dll")]
        private static extern long mciSendString(string comando, StringBuilder sb, int length, IntPtr cb);



    }
}
